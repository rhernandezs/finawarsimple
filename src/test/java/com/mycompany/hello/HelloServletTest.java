package com.mycompany.hello;
import com.mycompany.hello.HelloServlet;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;
import java.io.*;
import javax.servlet.http.*;
import org.apache.commons.io.FileUtils;
import org.junit.Test;

import org.junit.Test;

public class HelloServletTest {

	@Test
	public void testDoGet() throws Exception {
		HttpServletRequest request = mock(HttpServletRequest.class);       
           HttpServletResponse response = mock(HttpServletResponse.class);    



           StringWriter stringWriter = new StringWriter();
           PrintWriter writer = new PrintWriter(stringWriter);
           when(response.getWriter()).thenReturn(writer);

           new HelloServlet().doGet(request, response);

           //verify(request, atLeast(1)).getParameter("username"); // only if you want to verify username was called...
           writer.flush(); // it may not have been flushed yet...
		   assertTrue(stringWriter.toString().contains("Hello"));

	}

}
